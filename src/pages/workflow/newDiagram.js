export default {
	bpmnXml: '<?xml version="1.0" encoding="UTF-8"?>' +
			 '<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:camunda="http://camunda.org/schema/1.0/bpmn" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0wpmp9l" targetNamespace="流程" exporter="Camunda Modeler" exporterVersion="4.2.0">' +
				 '<bpmn:process id="Process_1" name="流程名称" isExecutable="true">' +
				 '</bpmn:process>' +
				 '<bpmndi:BPMNDiagram id="BPMNDiagram_1">' +
					'<bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">' +
					'</bpmndi:BPMNPlane>' +
				 '</bpmndi:BPMNDiagram>' +
			 '</bpmn:definitions>'
}